This game is a working project!
--------------------------------------------------------------------------
Hi!

This is my firts game.
--------------------------------------------------------------------------

Here are the steps to play:
--------------------------------------------------------------------------
-Install Arduino IDE, the Gamebuino Board and the library following this
tutorial:
    https://gamebuino.com/creations/gamebuino-meta-setup

-Clone or Download this repo.

-Open pong-do-leonel.ino and generate .bin file following this tutorial:
    https://gamebuino.com/creations/how-to-export-games

Now you have to put the game on your Gamebuino SD Card, or you can upload the
game to your Gamebuino from Arduino IDE.

If you still don't have a Gamebuino, you can see the game on Emlulator:
    https://gamebuino.com/creations/meta-emulator

Enjoy games at full speed on the Gamebuino META! (Don't use lights and sound yet).
Emulator by aoneill

Any questions, let me know
