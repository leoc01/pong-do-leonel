#include <Gamebuino-Meta.h>

//game speed
#define gspeed 2

//ball size
#define ball_size 2

//pads size
int pad_dimX=2;
int pad_dimY=10;

Gamebuino_Meta::Color pad[80][64]={{BLACK, BLACK},{BLACK, BLACK},{BLACK, BLACK},{BLACK, BLACK},{BLACK, BLACK},{BLACK, BLACK},{BLACK, BLACK},{BLACK, BLACK},{BLACK, BLACK},{BLACK, BLACK}};

//player pad position at beginning
int pad1_posX=4;
int pad1_posY=gb.display.height()/2-pad_dimY/2;

//computer pad position at beginning
int pad2_posX=gb.display.width()-pad1_posX-pad_dimX;
int pad2_posY=gb.display.height()/2-pad_dimY/2;

//ball position at beginning (with player)
int ball_posX=pad1_posX+pad_dimX;
int ball_posY=gb.display.height()/2-ball_size/2;

//ball speed at beginning
int ball_speedX=-gspeed;
int ball_speedY=gspeed;

//points counter
int player_points=0;
int computer_points=0;

//function to draw any objects(to future implemantion of custom colors on paddles and ball)
void draw(Gamebuino_Meta::Color object[80][64], int pos_x, int pos_y, int size_x, int size_y);

void setup() {
  gb.begin();
}

void loop() {
  while(!gb.update());
  gb.display.clear();

  //set backgroud collor
  gb.display.fill(BEIGE);

  //computer points check
  if(ball_posX<0-ball_size){
    pad1_posY=gb.display.height()/2-pad_dimY/2;

    ball_posX=pad1_posX+pad_dimX;
    ball_posY=gb.display.height()/2-ball_size/2;

    ball_speedX=-gspeed;
    ball_speedY=gspeed;

    computer_points+=1;

    //reset when win (10 points)
    if (computer_points==10){
      computer_points=0;
      player_points=0;
    }
  }

  //player points check
  if(ball_posX>gb.display.width()){
    pad2_posY=gb.display.height()/2-pad_dimY/2;

    ball_posX=pad2_posX-ball_size;
    ball_posY=gb.display.height()/2-ball_size/2;

    ball_speedX=gspeed;
    ball_speedY=gspeed;

    player_points+=1;

    //reset when win (10 points)
    if (player_points==10){
      computer_points=0;
      player_points=0;
    }
  }  

  //pad, line and points color
  gb.display.setColor(BLACK);

  //drwaing middle line
  for(int y=3;y<=62;y=y+9)
    gb.display.fillRect(39, y, 2, 5);

  //writing points
  gb.display.setFontSize(2);
  gb.display.setCursor(28,8);
  gb.display.print(player_points);
  gb.display.setCursor(46,8);
  gb.display.print(computer_points);
  
  //drawing pads
  draw(pad, pad1_posX,pad1_posY, pad_dimX,pad_dimY);
  draw(pad, pad2_posX,pad2_posY, pad_dimX,pad_dimY);

  //player pad control
  if(gb.buttons.repeat(BUTTON_UP, 1))
    pad1_posY-=gspeed;
  if(gb.buttons.repeat(BUTTON_DOWN, 1))
    pad1_posY+=gspeed;
  //player pad vertical limits
  if(pad1_posY>=gb.display.height()-pad_dimY/2)
    pad1_posY=gb.display.height()-pad_dimY/2;
  if(pad1_posY<=-pad_dimY/2)
    pad1_posY=-pad_dimY/2;

  //computer pad control
  if(ball_posY>=pad2_posY+pad_dimY-ball_size)
    pad2_posY+=gspeed;
  if(ball_posY<=pad2_posY)
    pad2_posY-=gspeed;
  
  //computer pad vertical limits
  if(pad2_posY>=gb.display.height()-pad_dimY/2)
    pad2_posY=gb.display.height()-pad_dimY/2;
  if(pad2_posY<=-pad_dimY/2)
    pad2_posY=-pad_dimY/2;

  //drawing ball
  gb.display.setColor(GREEN);
  gb.display.fillRect(ball_posX, ball_posY, ball_size, ball_size);

  //colision ball/player pad
  if((ball_posX==pad1_posX+pad_dimX)&&(ball_posY+ball_size>=pad1_posY)&&(ball_posY<=(pad1_posY+pad_dimY)))
    ball_speedX=-ball_speedX;

  //colision ball/computer pad
  if((ball_posX==pad2_posX-ball_size)&&(ball_posY+ball_size>=pad2_posY)&&(ball_posY<=(pad2_posY+pad_dimY)))
    ball_speedX=-ball_speedX;

  //ball vertical limits
  if(ball_posY>=gb.display.height()-ball_size||ball_posY<=0)
    ball_speedY=-ball_speedY;

  //ball position updating
  ball_posX=ball_posX+ball_speedX;
  ball_posY=ball_posY+ball_speedY;
}

void draw(Gamebuino_Meta::Color object[80][64], int pos_x, int pos_y, int size_x, int size_y){
  for(int i=0; i<size_x; i++){
    for(int j=0; j<size_y; j++){
      gb.display.setColor(object[i][j]);
      gb.display.fillRect(i+pos_x, j+pos_y, 1, 1);
    }
  }
}
